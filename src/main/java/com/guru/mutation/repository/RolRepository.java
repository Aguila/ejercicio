package com.guru.mutation.repository;

import com.guru.mutation.entity.Adn;
import com.guru.mutation.entity.Rol;
import com.guru.mutation.entity.Usuario;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RolRepository extends CrudRepository<Rol,Long> {

    List<Rol> findByUsuarios(Usuario user);

    Rol findByName(String name);

}
