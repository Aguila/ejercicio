package com.guru.mutation.repository;

import com.guru.mutation.entity.Adn;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdnRepository extends CrudRepository<Adn,Integer> {
}
