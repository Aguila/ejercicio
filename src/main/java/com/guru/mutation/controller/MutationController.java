package com.guru.mutation.controller;

import com.guru.mutation.model.DnaModel;
import com.guru.mutation.service.MotationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;

@RestController
public class MutationController {

    @Autowired
    private MotationService service;


    @PostMapping("/mutation")
    @Operation(security = {@SecurityRequirement(name = "bearer-key")})
    public String mutation(@RequestBody DnaModel dna) {
        System.out.println(dna.getDna().toString());
        service.saveAdn(dna.getDna());
        return "exito";
    }


}
