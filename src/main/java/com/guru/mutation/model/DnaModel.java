package com.guru.mutation.model;

import java.util.List;

public class DnaModel {
    List<String> dna;

    public List<String> getDna() {
        return dna;
    }

    public void setDna(List<String> dna) {
        this.dna = dna;
    }

    @Override
    public String toString() {
        return "DnaModel{" +
                "dna=" + dna +
                '}';
    }
}
