package com.guru.mutation.service;

import com.guru.mutation.entity.Adn;
import com.guru.mutation.repository.AdnRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MotationService {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private AdnRepository repository;

    public void saveAdn(List<String> params) {
        Adn adn = null;
        for (String param : params) {
            adn = new Adn();
            adn.setAdn(param);
            LOGGER.info("valid: "+validAdn(param));
            repository.save(adn);
        }
    }

    public boolean validAdn(String param) {
        String pattern = "[ATCG]";
        return param.matches(pattern);
    }
}
