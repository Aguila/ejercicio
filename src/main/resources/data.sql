INSERT INTO usuario (id, username, password) VALUES (1, 'admin', 'admin');
INSERT INTO usuario (id, username, password) VALUES (2, 'operador', 'operador');
INSERT INTO usuario (id, username, password) VALUES (3, 'ciudadano', 'ciudadano');

INSERT INTO rol (id,name) VALUES (1,'admin');
INSERT INTO rol (id,name) VALUES (2,'operador');
INSERT INTO rol (id,name) VALUES (3,'ciudadano');


INSERT INTO usuario_rol (usuario_id, rol_id) VALUES (1,1);
INSERT INTO usuario_rol (usuario_id, rol_id) VALUES (2,2);
INSERT INTO usuario_rol (usuario_id, rol_id) VALUES (3,3);