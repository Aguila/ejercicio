# Ejercicio-full

Este ejercicio es una aplicacion spring-boot, postgrest sql e integrando CI/CU (gitlab-ci / heroku)

# Pre-Requisitos

- Java 8
- Maven 3.X.X
- Heroku-CLI
- Git
- Acceso al repositorio Gitlab


### Repositorio
```
https://gitlab.com/Aguila/ejercicio.git
```
Clonar el proyecto
```
git clone https://gitlab.com/Aguila/ejercicio.git
```
# Base de Datos

### Configuracion de la base de datos

En el archivo `application.properties` debe de contener la configuracion siguiente:


``` properties
## Postgresql Production ##
spring.datasource.url=jdbc:postgresql://ec2-3-216-181-219.compute-1.amazonaws.com:5432/d5645ogmms5i9u
spring.datasource.username=qkulbamlbscdfy
spring.datasource.password=772500b44378b21190971a271b2f496cf341ae483727d79edf59864b16e58e61
spring.datasource.hikari.minimum-idle=3
spring.datasource.initialization-mode=always
spring.datasource.validationQuery = SELECT 1
spring.datasource.testWhileIdle = true
spring.jpa.database-platform=org.hibernate.dialect.PostgreSQLDialect
spring.jpa.properties.hibernate.jdbc.lob.non_contextual_creation=true
spring.jpa.show-sql=true
```

## Heroku Cloud Database
```
https://data.heroku.com/
```
Parametros de conexion  la base de datos

![configuracion](images/heroku-databases-settings.png)

Base de datos
![bases de datos](images/heroku-databases.png)

Menu de la base de datos
![base de datos menu](images/heroku-databases-dasboard.png)

Tablas de la base de datos
![tablas](images/heroku-databases-dataclip.png)


## Gitlab CI/CD

Obtenemos la Heroku API-KEY en Heroku. Heroku Account > Account Settings > API Key


![Heroku api key](images/heroku-api-key.png)


Registramos la Heroku API KEY como variable de la CI/CD. vamos al repositorio > Configuracion > CI/CD > Variables

![Variable CI](images/gitlab-variable.png)

Archivo `.gitlab-ci.yml`
``` yaml
stages:
  - build
  - test
  - deploy

maven-build:
  image: maven:3-jdk-8
  stage: build
  script: "mvn package -B"

maven-test:
  image: maven:3-jdk-8
  stage: test
  script: "mvn test -B"

deploy:
  stage: deploy
  image: ruby:latest
  script:
  - apt-get update -qy
  - apt-get install -y ruby-dev
  - gem install dpl
  - dpl --provider=heroku --app=ejercicio-guru --api-key=$HEROKU_API_KEY
  only:
  - master
  ```

```
https://gitlab.com/Aguila/ejercicio/-/pipelines
```

![production app](images/gitlab-pipelines.png)


### Ejecucion Local
Despues de descargar el proyecto del repositorio, nos ubicamos a la raiz de nuestro proyecto y ejecutamos el siguiente comando:
``` sh
mvnw clean install spring-boot:run
```
**Observacion:** antes de ejecutar los comandos anteriores debemos de asegurarnos tener instalado **Java 8**


# Rest API

## Login local
```
http://localhost:8086/login
```
POST

Payload
``` json
{
"username": "admin",
"password": "admin"
}
```

![Login Swagger](images/swagger-login.png)

## Mutation local
```
http://localhost:8086/mutation
```
POST

Payload
``` json
{
"dna": [
        "ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"
      ]
}
```

![Login Swagger](images/swagger-mutation.png)

# Swagger

## Produccion (Heroku)
```
https://ejercicio-guru.herokuapp.com/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config
```
## Local
``` 
http://localhost:8086/swagger-ui/index.html?configUrl=/v3/api-docs/swagger-config
```

# Heroku Configuracion
Iniciamos sesion con el Heroku-CLI con las credenciales correspondientes

![inicio de sesion heroku](images/heroku-cli-login.png)

Creamos nuestra instancia heroku con  la Heroku-CLI

```
heroku create ejercicio-guru
```
![inicio de sesion heroku](images/heroku-create-app.png)
